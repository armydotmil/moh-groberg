/*global $,Image,window,lightbox,document*/

function LightboxImages() {
    this.index = 0;
    this.start_w = $(window).width();
    if (this.start_w > 768) {
        this.images = $('.breakout-imgs img, .battle-imgs:last img, #ceremony img');
    } else {
        this.images = $('.breakout-imgs img, .battle-imgs:first img, #ceremony img');
    }
    this.len = this.images.length;
}

/**
 * Set up the click listner on the appropriate images
 */
LightboxImages.prototype.init = function() {
    var context = this;

    $('.enlarge').on('click', function(e) {
        e.preventDefault();
        context.onClick($(this).prev('img'));
    });

    if (context.start_w > 480) {
        this.images.on('click', function() {
            context.onClick($(this));
        });
    }

    context.onResize();
    context.keypress();
};

/**
 * Resize function - since we have two sets of battle images
 * we need to modify the images array and the click events
 * associated with them whenever the screen is resized
 */
LightboxImages.prototype.onResize = function() {
    var context = this, updated_arr = false, w;

    $(window).resize(function() {
        w = $(this).width();

        // down to mobile
        if (w <= 480 && context.start_w > 480) {
            updated_arr = false;
            context.start_w = w;
            context.images.off('click');

            // up to desktop
        } else if (w > 768 && context.start_w <= 768) {
            context.images = $('.breakout-imgs img, .battle-imgs:last img, #ceremony img');
            updated_arr = true;
            context.start_w = w;

        // down to tablet, up to tablet
        } else if ((w <= 768 && context.start_w > 768) ||
            (w > 480 && context.start_w <= 480)) {
            context.images = $('.breakout-imgs img, .battle-imgs:first img, #ceremony img');
            updated_arr = true;
            context.start_w = w;
        }

        // only mess with handlers if we need to
        if (updated_arr) {
            context.len = context.images.length;
            context.images.off('click');
            context.images.on('click', function() {
                context.onClick($(this));
            });
            updated_arr = false;
        }
    });
};

/**
 * Click event function
 * @param {element} elem
 */
LightboxImages.prototype.onClick = function(elem) {
    var context = this, margin, w = $(window).width();
    context.img = $(elem).parents('.inner-container, .full-inner-container').find('img');
    context.alt_text = context.img.prop('alt');
    context.index = context.images.index($(elem));

    // retrieve lightbox_image.html
    margin = w > 768 ? 15 : 0;
    $.get('lightbox_image.html', function(data) {
        $('#menu, .hamburger, body').css({
            'margin-right': margin,
            'overflow': 'hidden'
        });
        $('body').append(data);

        context.description = $('.lightbox_image p:first');
        context.lightBoxImage = $('.lightbox_image img');
        context.originalImage = $('.lightbox_image .download a');

        context.setDisplay(context.images.eq(context.index));

        // on click outside lightbox
        $('#lightbox_outer_content').click(function(e) {
            e.stopPropagation();
        });

        // on close
        $('#lightbox,.top_lightbox_buttons .lightbox_button').click(function() {
            $('#lightbox').remove();
            $('#menu, .hamburger, body').css({
                'margin-right': '',
                'overflow': 'auto'
            });

            $('.lightbox_image p:first').html('');

            $('.lightbox_image h2').text('');

            context.index = 0;
        });

        // on click right
        $('.lightbox_button_wrap_right .bottom_lightbox_buttons').click(function() {
            context.goRight();
        });

        // on click left
        $('.lightbox_button_wrap_left .bottom_lightbox_buttons').click(function() {
            context.goLeft();
        });
    });
};

/**
 * DRY code used in two places
 */
LightboxImages.prototype.goRight = function() {
    var context = this;

    if (context.index < context.len - 1) context.index++;

    context.setDisplay(context.images.eq(context.index));
};

/**
 * DRY code used in two places
 */
LightboxImages.prototype.goLeft = function() {
    var context = this;

    if (context.index > 0) context.index--;

    context.setDisplay(context.images.eq(context.index));
};

/**
 * Sweet little function
 * to move back and forth between images with arrow keys
 * and to close the lightbox with the escape key
 */
LightboxImages.prototype.keypress = function() {
    var context = this;

    $(document).keydown(function(e) {
        if ($('#lightbox').length) {

            // 27 = escape
            if (e.keyCode === 27) {
                lightbox.destroy();

            // 37 = left
            } else if (e.keyCode === 37) {
                context.goLeft();

            // 39 = right
            } else if (e.keyCode === 39) {
                context.goRight();
            }
        }
    });
};

/**
 * Update the lightbox with the
 * appropiate content
 * @param {element} elem
 */
LightboxImages.prototype.setDisplay = function(elem) {
    var newImage = new Image(),
        context = this,
        parentElem = elem.parents('.inner-container, .full-inner-container'),
        imgSrc;

    if ($(window).width() > 480) {
        if (this.index === 0) {
            $('.lightbox_button_wrap_left .bottom_lightbox_buttons').hide();
        } else if (this.index === this.len - 1) {
            $('.lightbox_button_wrap_right .bottom_lightbox_buttons').hide();
        } else {
            $('.lightbox_button_wrap_left .bottom_lightbox_buttons').show();
            $('.lightbox_button_wrap_right .bottom_lightbox_buttons').show();
        }
    } else {
        $('.bottom_lightbox_buttons').css('display', '');
    }

    this.img = $(parentElem).find('img');
    this.alt_text = this.img.prop('alt');
    this.description.text(this.alt_text);

    newImage.onload = function() {
        imgSrc = this.src.match('_480') || this.src.match('460') ?
            this.src.replace('_480', '').replace('_460', '') :
            this.src;
        context.lightBoxImage.prop('src', imgSrc.replace('.jpg', '_660.jpg'));
    };
    newImage.src = this.img.prop('src');

    if (this.img.prop('src').match('bio')) {
        imgSrc = this.img.prop('src');
    } else if (this.img.prop('src').match('_480.jpg')) {
        imgSrc = this.img.prop('src').replace('_480.jpg', '_original.jpg');
    } else if (this.img.prop('src').match('_460')) {
        imgSrc = this.img.prop('src').replace('_460', '');
    } else {
        imgSrc = this.img.prop('src').replace('.jpg', '_original.jpg');
    }

    this.originalImage.prop('href', imgSrc);
};
