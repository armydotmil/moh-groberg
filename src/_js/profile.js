/*global window,$,document*/


function get_url_param(param) {
    var i = 0, page_url = window.location.search.substring(1),
    param_name, url_vars = page_url.split('&');
    for (i; i < url_vars.length; i++) {
        param_name = url_vars[i].split('=');
        if (param_name[0] == param) return param_name[1];
    }
}

$(document).ready(function() {
    var day_of, day_of_hoh, day_of_wh,
    hoh_end_time, hoh_start_time,
    now = new Date(), override = get_url_param('t'),
    wh_end_time, wh_start_time;

    //0000 of the day of the ceremony (ET)
    day_of_wh = 1447304400000;

    //One hour before WH scheduled start time
    wh_start_time = 1447341300000;  // 11/12/15 1015 am
    //one hour after WH scheduled end time
    wh_end_time = 1447349400000;  // 11/12/15 1230 pm

    day_of_hoh = 1447390800000;

    //One hour before HOH scheduled start time
    hoh_start_time = 1447423200000;
    //One hour after HOH scheduled end time
    hoh_end_time = 1447430400000;

    override = parseInt(override, 10);

    now = !isNaN(override) ? override : now.getTime();
 
    if (now > day_of_wh && now < wh_start_time) {
        stream_ad.init(
            "Live Webcast: Medal of Honor Ceremony",
            "Watch the Medal of Honor ceremony live webcast today at 11:15 a.m. EST",
            "President Barack Obama will award the Medal of Honor to Captain Florent Groberg today, at the White House. Groberg will be awarded the Medal of Honor for his actions in Afghanistan.",
            "/e2/rv5_images/medalofhonor/groberg/stream_ads/white_house.jpg",
            "The White House"
        );
    } else if (now > wh_start_time && now < wh_end_time) {
        $('.masthead_container:first').hide();
        live_stream.init(
            "Live Webcast: Medal of Honor Ceremony",
            999 // pass 999 to use wh live stream
        );
    } else if (now > wh_end_time && now < hoh_start_time) {
        day_of = (now < day_of_hoh) ? 'tomorrow' : 'today';
        // test day_of with time = wh_end_time + 1 to see 'tomorrow'
        // test day_of with time = day_of_hoh to see 'today'
        stream_ad.init(
            "Live Webcast: Hall of Heroes Induction",
            "Watch the Hall of Heroes Induction Ceremony live webcast, " + day_of + " at 10 a.m. EST",
            "The U.S. Army will induct Capt. Florent Groberg into the Pentagon's Hall of Heroes " + day_of + " at 10 a.m. EST. The Pentagon ceremony will add Groberg's name to the distinguished roster in the Hall of Heroes, the Defense Department's permanent display of record for all recipients of the Medal of Honor.",
            "/e2/rv5_images/medalofhonor/groberg/stream_ads/hall_of_heroes.jpg",
            "The Pentagon"
        );
    } else if (now > hoh_start_time && now < hoh_end_time) {
        $('.masthead_container:first').hide();
        live_stream.init(
            "Live Webcast: Hall of Heroes Induction",
            1
        );
    }
});

var stream_ad = {

    // test WH ceremony with (day of): 1447304400001
    // test HOH ceremony with (day of): 1447390800001

    init : function(maintitle, subtitle, message, image, alt) {
        var stream_ad = $('#stream-ad'),
        stream_info = $('#stream-info'),
        stream_info_left, stream_info_right;

        stream_ad.show(); // since it is hidden by default
        stream_ad.find('.section-title h2').text(maintitle);

        stream_info = $('#stream-info');

        stream_info_left = $('<div>').prop({
            'id' : 'stream-info-left'
        });

        stream_info_left.append(
            $('<blockquote>').text(subtitle)
        );

        stream_info_left.append(
            $('<p>').text(message)
        );

        stream_info_right = $('<div>').prop({
            'id' : 'stream-info-right'
        });

        stream_info_right.append(
            $('<img>').prop({
                'src' : image,
                'alt' : alt
            })
        );

        stream_info.append(stream_info_left).append(stream_info_right);
    }
};

var live_stream = {

    // test live stream WH with: 1447341300001
    // test live stream HOH with: 1447423200001

    init: function(maintitle, stream_id) {

        var player_w = "",
        player_h = "";

        $('#stream-ad').show();

        $('#stream-ad .section-title h2').text(maintitle);

        $('#stream-info').prepend(
            $('<div>').prop({
                'id' : 'live-stream-feed'
            }).append(
                $('<div>').prop({
                    'class' : 'player'
                })
            )
        );

        if (stream_id !== 999) {
            /*$('#stream-info').html('').append(
                $('<div>').prop({
                    'id' : 'dvids_ad'
                }).append(
                    $('<div>').prop({
                        'id' : 'dvids_inner'
                    })
                )
            );*/
            
            player_w = parseInt($('#live-stream-feed').width(), 10);
            // if (player_w > 730) player_w = 730;

            player_h = Math.floor(player_w * 9 / 16);

            $('#live-stream-feed .player').append(
                '<iframe width="' + player_w + '" height="' + player_h +'" ' +
                'src="http://media.dma.mil/embed/live.html?stream=' + stream_id +
                '&amp;width=' + player_w + '&amp;height=' + player_h +
                '&amp;autoplay=1" frameborder="0" scrolling="no" style="overflow:' +
                'hidden;"></iframe>'
            );
        } else {
            $('#live-stream-feed .player').append(
                '<iframe width="100%" height="100%" src="//www.youtube.com/embed/81GKGWGro4E?wmode=transparent" frameborder="0" wmode="Opaque" allowfullscreen></iframe>'
            );
        }

            /*$('#dvids_inner').append(
                $('<p>').html('VIDEO PROVIDED BY <a href="http://www.dvidshub.net/"><img src="/e2/rv5_images/medalofhonor/valor24/dvids_logo.png" alt="dvids" /></a>')
            );*/
    }
};



