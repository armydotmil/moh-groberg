/*global $,window,Modernizr,document,ga,FastClick,LightboxImages*/

var menu = {
    show_menu: function() {
        var menu_class = '';

        if ($('#menu').hasClass('open')) {
            $('.hamburger').removeClass('active');
            $('body').css('overflow', 'auto');
            $('#menu').removeClass('open half');
        } else {
            menu_class = ($(window).width() > 480) ? ' half' : '';
            $('body').css('overflow', 'hidden');
            $('#menu').addClass('open' + menu_class);
        }
    },

    events: function() {
        //If menu is open and user clicks outside of the menu,
        //close the menu

        $('html').on('click', function() {
            if ($('#menu').hasClass('open')) menu.show_menu();
        });

        //Unless click is inside menu
        $('#menu').on('click', function(e) {
            e.stopPropagation();
        });

        $('#menu_inner').on('click', 'h4 a', function() {
            var off_set = 50, target = $(this.hash), w = $(window).width();

            target = target.length ?
                target :
                $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {

                // to compensate for the header bar
                if (w < 993)
                    off_set = target.offset().top < position ? 66 : 0;

                $('html,body').animate({
                    scrollTop: target.offset().top - off_set
                }, 1000);
                if (w < 993) menu.show_menu();

                return false;
            }
        });

        $('.hamburger').on('click', function() {
            if (!$(this).hasClass('active')) {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }
            menu.show_menu();
            return false;
        });
    }
};

var fallen = {
    images: function(size) {
        var i = 0, teammates = $('.teammate img'), old_src;

        for (i; i < teammates.length; i++) {
            old_src = $('.teammate img').eq(i).prop('src');
            old_src = old_src.match('_460') ?
                old_src.replace('_460.jpg', '') :
                old_src.replace('.jpg', '');
            $('.teammate img').eq(i).prop('src', old_src + size + '.jpg');
        }
    }
};

var bio_map = {
    images: function(size) {
        var i = 0,
        imgs = $('.change-on-resize img'),
        old_src;

        for (i; i < imgs.length; i++) {
            old_src = $(imgs[i]).prop('src');

            old_src = old_src.match('_480') ?
                old_src.replace('_480.jpg', '') :
                old_src.replace('.jpg', '');

            $(imgs[i]).prop('src', old_src + size + '.jpg');
        }
    }
};

var lightbox = {
    init: function() {
        var lb = $('<div>').prop({
            'id': 'lightbox',
            'class': 'video'
        }),
        outer_container = $('<div>').prop({
            'class': 'container'
        }),
        inner_container_wrap = $('<div>').prop({
            'class': 'lightbox-outer-video-wrap'
        }),
        inner_container = $('<div>').prop({
            'class': 'lightbox-video-wrap'
        }),
        margin = 0;

        outer_container.append(
            inner_container_wrap.append(
                inner_container
            )
        );

        lb.click(function() {
            lightbox.destroy();
        });

        margin = $(window).width() > 768 ? 15 : 0;
        $('#menu, .hamburger, body').css({
            'margin-right': margin,
            'overflow': 'hidden'
        });
        $('body').append(lb.append(outer_container));

        lightbox.video();
    },
    destroy: function() {
        $('#lightbox').remove();
        $('#menu, .hamburger, body').css({
            'margin-right': '',
            'overflow': 'auto'
        });
    },
    video: function() {
        var h, w = $('#lightbox .container').width();

        h = Math.floor(w * 9 / 16);

        $('#lightbox .lightbox-video-wrap').append(
            '<iframe width="' + w + '" height="' + h +
            '" src="//www.youtube.com/embed/gblflu_CKqo?autoplay=1' +
            '&autohide=1&fs=1&rel=0&hd=1&wmode=opaque&enablejsapi=1"' +
            ' frameborder="0" allowfullscreen></iframe>'
        );
    }
};

var position = $(window).scrollTop();

$(window).scroll(function() {
    var buffer = $('.masthead_buffer').height(),
    collapse_point = 30,
    divs = [$('#biography'), $('#battle'), $('#team'), $('#army-news')],
    i = 0,
    scroll_top = $(window).scrollTop(),
    w = $(window).width();

    if (scroll_top > collapse_point && w > 992) {
        $('#top_bar, #menu').addClass('collapsed');
        $('#featureBar').addClass('fb_collapsed');
    } else {
        $('#top_bar, #menu').removeClass('collapsed');
        $('#featureBar').removeClass('fb_collapsed');
    }

    if (scroll_top > position &&
        scroll_top > buffer &&
        w < 993) {
        // down
        $('#top_bar').css('top', '-100px');
    } else {
        // up
        $('#top_bar').css('top', '');
    }
    position = scroll_top;

    for (i; i < divs.length; i++) {
        if (scroll_top >= divs[i].offset().top - 100) {
            $('#menu_inner div').removeClass('active');
            $('#menu_inner div').eq(i).addClass('active');
        }
    }
})
.resize(function() {
    var bio_src = $('.change-on-resize:last img').prop('src'),
    h = 0, img_size = '', need_new_img = false,
    set_src = $('.teammate:first img').prop('src'),
    w = $(this).width();

    if (w > 768 && set_src.match('460')) {
        need_new_img = true;
    } else if (w < 769 && !set_src.match('460')) {
        need_new_img = true;
        img_size = '_460';
    }

    if (w > 480 && bio_src.match('480')) {
        bio_map.images('');
    } else if (w <= 480 && !bio_src.match('480')) {
        bio_map.images('_480');
    }

    // only change the image if we need to
    if (need_new_img) fallen.images(img_size);

    if (w > 992) {
        $('#menu').css({
            'right': '',
            'width' : ''
        });
        $('#top_bar').css('top', '');
        $('#fallen .teammate').height('');
        if ($('#menu').hasClass('open')) menu.show_menu();
    } else if (w < 769 && w > 480) {
        $('#fallen .teammate').height('');
        h = $('#fallen .teammate').height();
        $('#fallen .teammate').height(h);
    } else {
        $('#fallen .teammate').height('');
    }

});

$(document).ready(function() {
    var h = 0, img_size,
    margin = 0, w = $(window).width();

    FastClick.attach(document.body);

    menu.events();

    if (position > 30 && w > 992) {
        $('#top_bar, #menu').addClass('collapsed');
        $('#featureBar').addClass('fb_collapsed');
    }

    /*
     * Detect high res displays (Retina, HiDPI, etc...)
     *
     * Good stuff, but not in use
     *
    Modernizr.addTest('highresdisplay', function() {
        if (window.matchMedia) {
            var mq = window.matchMedia('only screen and (-moz-min-device-pixel-ratio: 1.3), only screen and (-o-min-device-pixel-ratio: 2.6/2), only screen and (-webkit-min-device-pixel-ratio: 1.3), only screen and (min-device-pixel-ratio: 1.3), only screen and (min-resolution: 1.3dppx)');
            if (mq && mq.matches) {
                return true;
            }
        }
    });*/

    if (!Modernizr.backgroundsize)
        $('.full_width_image').css({'background-size': 'contain'});

    img_size = w > 768 ? '' : '_460';
    fallen.images(img_size);
    img_size = w > 480 ? '' : '_480';
    bio_map.images(img_size);

    $('a').prop('target', '_blank');

    if (w < 769 && w > 480) {
        $('#fallen .teammate').height('');
        h = $('#fallen .teammate').height();
        $('#fallen .teammate').height(h);
    } else {
        $('#fallen .teammate').height('');
    }

    $('.social').on('click', 'a', function() {
        window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
        return false;
    });

    $('.unit-info').on('click', function(e) {
        e.preventDefault();
        margin = w > 768 ? 15 : 0;
        $.get('unit.html', function(data) {
            $('#menu, .hamburger, body').css({
                'margin-right': margin,
                'overflow': 'hidden'
            });
            $('body').append(data);

            $('#lightbox_outer_content').click(function(e) {
                e.stopPropagation();
            });

            $('#lightbox,.lightbox_button').click(function() {
                $('#lightbox').remove();
                $('#menu, .hamburger, body').css({
                    'margin-right': '',
                    'overflow': 'auto'
                });
            });
        });
    });

    $('#citation').on('click', function(e) {
        e.preventDefault();
        margin = w > 768 ? 15 : 0;
        $.get('citation.html', function(data) {
            $('#menu, .hamburger, body').css({
                'margin-right': margin,
                'overflow': 'hidden'
            });
            $('body').append(data);

            $('#lightbox_outer_content').click(function(e) {
                e.stopPropagation();
            });

            $('#lightbox,.lightbox_button').click(function() {
                $('#lightbox').remove();
                $('#menu, .hamburger, body').css({
                    'margin-right': '',
                    'overflow': 'auto'
                });
            });
        });
    });

    $('#battle_video').on('click', 'a', function(e) {
        e.preventDefault();
        lightbox.init();
    });

    $('#ceremony').on('click', '.show-hide-caption', function() {
        if (!$(this).parents('.show-caption').length) {
            $(this).parents('.inner-container').addClass('show-caption');
            $(this).html('Hide Caption &ndash;');
        } else {
            $(this).parents('.inner-container').removeClass('show-caption');
            $(this).text('Show Caption +');
        }
    });

    new LightboxImages().init();

    //send an event to GA when a link is clicked
    $('a').on('click', function() {
        if ($(this).attr('href').match(/johnson/g) ||
            $(this).attr('href').match(/#/g) &&
            $(this).attr('href') !== '#') {
            var dest_uri = $(this).attr('href').split('/');
            var current_uri = window.location.pathname.split('/');
            var i = dest_uri.length - 1;
            var j = current_uri.length - 1;

            var dest_pagename = dest_uri[i - 1];
            var current_pagename = current_uri[j - 1];
            var section_name = current_pagename;
            var send_event = true;

            if ($.inArray('#', dest_uri[i]) > -1)
                dest_pagename = dest_uri[i].split('#')[1];

            if (current_pagename.match(/.html/g))
                current_pagename = current_uri[j - 2];

            if ($(this).parents('#menu_inner').length)
                section_name = 'menu';

            var description = current_pagename + '_' + dest_pagename;

            if (send_event && typeof ga === 'function')
                ga('send', 'event', section_name, 'click', description);

        }
    });

});
